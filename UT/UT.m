//
//  UT.m
//  UT
//
//  Created by alex on 12/15/12.
//  Copyright (c) 2012 alex. All rights reserved.
//

#import "UT.h"

@implementation UT

- (void)setUp {
    [super setUp];
    _parser = [[Parser alloc] initWithDefaults];
    _payment = [[Payment alloc] init];
    _mock = [OCMockObject mockForClass:[Parser class]];
}

- (void)tearDown {
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testcheckIsInit {
    STAssertNotNil(_parser, @"Can't create object of %@", self.name);
}

- (void)testRaisesExceptionWhenUnknownMethodIsCalled
{
    STAssertThrows([_mock uppercaseString], @"Should have raised an exception.");
}

- (void)testLoadDataFromFile {
    NSURL *path = [NSURL fileURLWithPath:@"/Users/alexa/Documents/Projects/parseCSV/UT/data.csv"];
    if([[NSFileManager defaultManager] fileExistsAtPath:[path path]]) {
        NSString *ret1 = [_parser readDataFrom:path];
        NSString *ret2 = [NSString stringWithContentsOfURL:path encoding:NSISOLatin1StringEncoding error:nil];
        STAssertEquals(ret1.length, ret2.length, @"Both should be eqauls.");
    } else {
        STFail(@"File can not be found."); 
    }
}

- (void)testParsersPropertyShouldBePopulatedByData {
    
    // Init standard data
    NSMutableArray *exludeList = [NSMutableArray arrayWithObjects:@"Teenustasu", @"Hooldustasu", nil];
    NSCharacterSet *delimiters = [NSCharacterSet characterSetWithRange:NSRangeFromString(@";")];
    NSMutableArray *reservedWords = [NSMutableArray arrayWithObjects:@"Seesami", @"Amazon", @"Apple", nil];
    
    STAssertEquals(_parser.exludeList.count, exludeList.count, @"Default data should be same");
    STAssertEquals(sizeof(_parser.delimiters), sizeof(delimiters), @"Default data should be same");
    STAssertEquals(sizeof(_parser.reservedWords), sizeof(reservedWords), @"Default data should be same");
}

- (void)testRaisesExceptionWhenWrongPathUsed {
    NSURL *path = [NSURL fileURLWithPath:@"../Text/data.txt"];    
    STAssertThrows([_mock readDataFrom:path], @"Incorrect path: '%s'", [path path]);
}

- (void)testRaisesExceptionWhenPathIsEmpty {
    NSURL *path = [NSURL fileURLWithPath:@""];
    STAssertThrows([_mock readDataFrom:path], @"Path: '%s' is wrong.", [path path]);
}

- (void)testPaymentShouldBeSame {
    NSString *testLine = @"\"10010078797011\";"";\"02.01.2010\";"";\"SEB\";\"401\";"";\"D\";        442,56;"";\"RO850084415\";\"31/12/2009 12:19 kaart...684467 Hyper Rimi Haabersti   #544403\";0;\"EEK\";";
    Payment *pay1 = [[Payment alloc] initFromArray:[testLine componentsSeparatedByString:@";"]];
    Payment *pay2 = [_parser parseLine:testLine];
    STAssertEquals(pay1.beneficiaryAccount, pay2.beneficiaryAccount, @"Should be equals.");
    STAssertEquals([pay1.amount doubleValue], [pay2.amount doubleValue], @"Should be equals.");
}

- (void)testRaisesEsceptionIfArgumentIsNilOrEmpty {
    STAssertNil([_parser parseLine:@""], @"Exception should be trow.");
    STAssertNil([_parser parseLine:nil], @"Exception should be trow.");
}

@end
