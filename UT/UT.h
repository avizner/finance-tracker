//
//  UT.h
//  UT
//
//  Created by alex on 12/15/12.
//  Copyright (c) 2012 alex. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import <OCMock/OCMock.h>
#import "Parser.h"

@interface UT : SenTestCase {
    @private
    Parser      *_parser;
    Payment     *_payment;
    id          _mock;
}

@end
