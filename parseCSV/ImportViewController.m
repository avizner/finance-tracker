//
//  ImportViewController.m
//  parseCSV
//
//  Created by alex on 12/21/12.
//  Copyright (c) 2012 alex. All rights reserved.
//

#import "AppDelegate.h"
#import "ImportViewController.h"

@implementation ImportViewController

@synthesize label;

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self registerForDraggedTypes:[NSArray arrayWithObject:NSURLPboardType]];
    }
    return self;
}

- (void)awakeFromNib {
    //bgImage = [[NSImage alloc] initWithContentsOfFile:@"/Users/alexa/Documents/Projects/parseCSV/png.png"];
}

- (void)drawRect:(NSRect)dirtyRect
{
    NSColor *color = [NSColor colorWithDeviceRed:222/255.0f green:225/255.0f blue:233/255.0f alpha:1.0];
    [color setFill];
    NSRectFill(dirtyRect);
}

- (NSDragOperation)draggingEntered:(id<NSDraggingInfo>)sender {
   
    NSPasteboard *pboard = [sender draggingPasteboard];
    NSArray *classes = [[NSArray alloc] initWithObjects:[NSURL class], nil];
    NSDictionary *options = [NSDictionary dictionaryWithObject:
                             [NSNumber numberWithBool:YES] forKey:NSPasteboardURLReadingFileURLsOnlyKey];
    
    
    ok = ([pboard readObjectsForClasses:classes options:options] != nil);
    
    if (ok) {
        
        urls = [pboard readObjectsForClasses:classes options:options];
        NSLog(@"draggingEntered: %@", urls);
        AppDelegate * delegate =(AppDelegate*) [NSApplication sharedApplication].delegate;
        [delegate initParsing:urls];
        highlight = YES;
        [self setNeedsDisplay:YES];
        return NSDragOperationCopy;
    }
    return NSDragOperationNone;
}

- (void)draggingExited:(id<NSDraggingInfo>)sender {
    highlight = NO;
    [self setNeedsDisplay:YES];
}

- (BOOL)prepareForDragOperation:(id<NSDraggingInfo>)sender {
    
    highlight = NO;
    [self setNeedsDisplay:YES];
    return (ok);
}

- (BOOL)performDragOperation:(id<NSDraggingInfo>)sender {
    
    if ([sender draggingSource] != self) {
        
        
        
    }
    
    return YES;
}

- (NSDragOperation)draggingSession:(NSDraggingSession *)session sourceOperationMaskForDraggingContext:(NSDraggingContext)context
{
    switch (context) {
        case NSDraggingContextOutsideApplication:
            return NSDragOperationCopy;
            
            //by using this fall through pattern, we will remain compatible if the contexts get more precise in the future.
        case NSDraggingContextWithinApplication:
        default:
            return NSDragOperationCopy;
            break;
    }
}

- (void)pasteboard:(NSPasteboard *)sender item:(NSPasteboardItem *)item provideDataForType:(NSString *)type
{
    //sender has accepted the drag and now we need to send the data for the type we promised
    if ( [type compare: NSPasteboardTypeRTF] == NSOrderedSame
        || [type compare: NSPasteboardTypeString] == NSOrderedSame ) {
        
        //set data for TIFF type on the pasteboard as requested
        
        
    } else if ( [type compare: NSPasteboardTypePDF] == NSOrderedSame ) {
        
        //set data for PDF type on the pasteboard as requested
        [sender setData:[self dataWithPDFInsideRect:[self bounds]] forType:NSPasteboardTypePDF];
    }
    
}

@end
