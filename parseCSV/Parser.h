//
//  Parser.h
//  parseCSV
//
//  Created by alex on 11/24/12.
//  Copyright (c) 2012 alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Parser.h"
#import "Payment.h"

@interface Parser : NSObject {
    NSEnumerator    *_paths;
    NSMutableArray  *_exludeList;
    NSCharacterSet  *_delimiters;
}

@property NSEnumerator      *paths;
@property NSMutableArray    *exludeList;
@property NSCharacterSet    *delimiters;
@property NSMutableArray    *reservedWords;

- (id)init;
- (id)initWithDefaults;

- (Payment*)parseLine:(NSString*)line;
- (NSArray*)startParsing:(NSString*)inputData;
- (NSString*)readData;
- (NSString*)readDataFrom:(NSURL*)url;
- (NSArray*)searchAndSwapBeneficName:(NSArray*)inputData;
- (NSString*)getBeneficiaryNameFromString:(NSString*)inputString;

@end

@interface NSMutableArray (ContainsString)

- (BOOL) containsStringLike:(NSString*)string matchedString:(NSString **)outString;
- (BOOL) containsStringLike:(NSString*)string;

@end
