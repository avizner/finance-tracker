//
//  Record.h
//  parseCSV
//
//  Created by alex on 12/5/12.
//  Copyright (c) 2012 alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Payment : NSObject

@property NSDate    *date;
@property NSString  *beneficiary;
@property NSNumber  *amount;
@property NSString  *description;

- (id)initFromArray:(NSArray*)array;

@end
