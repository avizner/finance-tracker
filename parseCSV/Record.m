//
//  Record.m
//  parseCSV
//
//  Created by alex on 12/5/12.
//  Copyright (c) 2012 alex. All rights reserved.
//

#import "Record.h"

@implementation Payment

@synthesize date;
@synthesize beneficiary;
@synthesize amount;
@synthesize description;

- (id)initFromArray:(NSArray*)array {
    if (self = [super init]) {
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"dd.mm.yyyy"];
        
        if(array != nil || array.count == 3) {
            NSString *tmp = [array[0] stringByTrimmingCharactersInSet:
                             [NSCharacterSet characterSetWithCharactersInString:@"\""]];
            
            date =          [dateFormatter dateFromString:tmp];
            beneficiary =   array[1];
            amount =        array[2];
            description =   array[3];
        }
        return nil;
    }
    return self;
}

@end
