//
//  Parser.m
//  parseCSV
//
//  Created by alex on 11/24/12.
//  Copyright (c) 2012 alex. All rights reserved.
//

#import "Parser.h"

@implementation Parser

@synthesize paths = _paths;
@synthesize exludeList = _exludeList;
@synthesize delimiters = _delimiters;


- (id)init {
    if (self = [super init]) {
        
    }
    return self;    
}

// TODO: Should be managed from UI
- (id)initWithDefaults {
    if (self = [super init]) {    
    _exludeList = [NSMutableArray arrayWithObjects:@"Teenustasu", @"Hooldustasu", nil];
    _delimiters = [NSCharacterSet characterSetWithRange:NSRangeFromString(@";")];
    }
    return self;
}

// Create payment from parsed line
- (Payment*)parseLine:(NSString*)line {
    
    // Skip very first line (header) by specific keyword
    // TODO recognizing strings encoding
    if (line || [line rangeOfString:@"BIC/SWIFT"].location == NSNotFound) {

        NSArray *arr = [self searchAndSwapBeneficName:[line componentsSeparatedByString:@";"]];
        if (arr || arr.count >= 15) {
            return [[Payment alloc] initWithArray:arr];
        }
    }
    return nil;
}

- (NSString*)getBeneficiaryNameFromString:(NSString*)inputString {
    if(inputString != nil){
        NSRange lastSearchIndex = [inputString rangeOfString:@"#"];
        NSString *pattern = @"[...][0-9]{6}";
        NSError *err;
        NSRegularExpression *regex =
        [NSRegularExpression regularExpressionWithPattern:pattern
                                                  options:NSRegularExpressionCaseInsensitive
                                                  error:&err];
        NSRange firstSearchIndex = [regex rangeOfFirstMatchInString:inputString
                                                            options:NSRegularExpressionCaseInsensitive
                                                              range:NSMakeRange(0, [inputString length])];
        
        if (firstSearchIndex.location != NSNotFound && lastSearchIndex.location != NSNotFound) {
            NSRange resultRange;
            resultRange.location = firstSearchIndex.location + firstSearchIndex.length;
            resultRange.length = lastSearchIndex.location - (firstSearchIndex.location + firstSearchIndex.length);
            if ([inputString length] >= (resultRange.location + resultRange.length)) {
                inputString = [inputString substringWithRange:resultRange];
            }
        } 
    }
    return inputString;
}

- (NSMutableArray*)searchAndSwapBeneficName:(NSMutableArray *)inputData {
    
    NSString *str;
    if (inputData.count >= 11
        && [inputData[4] rangeOfString:@"SEB"  options:NSCaseInsensitiveSearch].location != NSNotFound
        && ![_exludeList containsStringLike:inputData[11]]){
            
        str = [self getBeneficiaryNameFromString:inputData[11]];
        
        if (str) {
            [inputData replaceObjectAtIndex:4 withObject:str];
        }
    }
    
    return inputData;
}

- (NSMutableArray*)startParsing:(NSString*)inputData {
    
    NSMutableArray* set = [[NSMutableArray alloc]init];
    Payment *pay = nil;
    NSLog(@"Start parsing of document.");
    
    for (NSString *line in [inputData componentsSeparatedByString:@"\n"]) {
        pay = [self parseLine:[line stringByReplacingOccurrencesOfString:@"\"" withString:@""]];
        if (pay) {
            [set addObject:pay]; 
        }
    }
    
    return set;
}

- (NSString*)readData {
    
    NSMutableString *ret;
    for(NSURL* path in _paths) {
        [ret appendString:[self readDataFrom:path]];
    }
    
    return ret;
}

- (NSString*)readDataFrom:(NSURL *)url {
    
    NSString *ret = nil;
    if (![[NSFileManager defaultManager] fileExistsAtPath:[url path]]) {
        NSAlert *noSuchFile = [[NSAlert alloc]init];
        [noSuchFile setMessageText:@"No such file available."];
        [noSuchFile setInformativeText:@"Please check if file exists."];
        [noSuchFile runModal];
        [NSException raise:@"Parser:readDataFrom: No such file was found." format:nil];
    } else {
        NSError *err = nil;
        NSStringEncoding *enc = nil;
        
        ret = [[NSString alloc] initWithContentsOfURL:url usedEncoding:enc error:&err];
        
        if (err) {
            err = nil;
            ret = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&err];
        }
        
        if (err) {
            err = nil;
            ret = [NSString stringWithContentsOfURL:url encoding:NSISOLatin1StringEncoding error:&err];
        }
    }
    
    return ret;
}

@end

@implementation NSMutableArray (ContainsString)

- (BOOL)containsStringLike:(NSString*)string {
    
    return [self containsStringLike:string matchedString:nil];
}

- (BOOL)containsStringLike:(NSString *)string matchedString:(NSString *__autoreleasing *)outString {
    
    for (NSString* str in self) {
        if ([string rangeOfString:str options:NSCaseInsensitiveSearch].location != NSNotFound) {
            return YES;
        }
    }
    
    return NO;
}

@end
