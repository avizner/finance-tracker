
#import <Cocoa/Cocoa.h>

@interface CSVParser : NSObject

-(NSArray *)csvRows;

@end
