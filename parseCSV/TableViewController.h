//
//  TableViewController.h
//  parseCSV
//
//  Created by alex on 12/5/12.
//  Copyright (c) 2012 alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TableViewController : NSTableView <NSTableViewDataSource, NSTableViewDelegate> {
    IBOutlet NSTableView *tView;
    NSMutableArray *data;
}

- (id)init;
- (IBAction)add:(id)sender;

@end
