//
//  AppDelegate.m
//  parseCSV
//
//  Created by alex on 11/21/12.
//  Copyright (c) 2012 alex. All rights reserved.
//

#import <Quartz/Quartz.h>
#import "AppDelegate.h"
#import "Parser.h"
#import "TableViewController.h"
#import "ImportViewController.h"

@implementation AppDelegate

@synthesize data;
@synthesize filesPath;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    data = [[NSMutableArray alloc] init];
    [tableView setHidden:YES];
    viewContext = tableView;
}

- (IBAction)openExistingDocument:(id)sender {
    
    NSOpenPanel* panel = [NSOpenPanel openPanel];
    
    //    File selection: enabled
    [panel setCanChooseFiles: YES];
    
    //    Directory selection: disabled
    [panel setCanChooseDirectories: NO];
    
    //    Resolve aliases: enabled
    [panel setResolvesAliases: YES];
    
    //    Multiple selection: enabled
    [panel setAllowsMultipleSelection: YES];
    
    //    Show hidden files: disabled
    [panel setShowsHiddenFiles:NO];
    
    //    File packages treated as directories: disabled
    [panel setTreatsFilePackagesAsDirectories:NO];
    
    //    Can create directories: disabled
    [panel setCanCreateDirectories:NO];
    
    [panel beginWithCompletionHandler:^(NSInteger result){
        if (result == NSFileHandlingPanelOKButton)
            filesPath = [panel URLs] ;
    }];
}

+ (AppDelegate *)getAppDelegate
{
    return (AppDelegate *)[[NSApplication sharedApplication] delegate];
}

- (void)initParsing:(NSArray*)urls {

    NSString *inputData = nil;
    if(urls != nil) {
        for(NSURL* url in urls) {
            
            Parser *psr = [[Parser alloc]initWithDefaults];
            
            if (url != nil) {
                inputData = [psr readDataFrom:url];
            }
            
            if (inputData != nil) {
                [data addObjectsFromArray:[psr startParsing:inputData]];
            }            
        }
    }
    else
    {
        [self showNoFileSeletedDialog];
    }
}

- (IBAction)showTableView:(id)sender {    
    [importView setHidden: YES];
    [tableView setHidden:(tableView.isHidden) ? NO : YES];
}

- (IBAction)showImportView:(id)sender {
    [tableView setHidden:YES];
    [importView setHidden:(importView.isHidden) ? NO : YES];
}

- (void)showNoFileSeletedDialog {
    
    NSAlert* noFileSeleted = [[NSAlert alloc] init];
    [noFileSeleted addButtonWithTitle:@"OK"];
    [noFileSeleted setMessageText:@"Warning"];
    [noFileSeleted setInformativeText:@"No files for parsing were selected."];
    [noFileSeleted setAlertStyle:NSWarningAlertStyle];
    [noFileSeleted runModal];
}

- (void)showSettingsView:(id)sender {
    
}

@end
