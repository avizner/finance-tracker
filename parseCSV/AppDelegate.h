//
//  AppDelegate.h
//  parseCSV
//
//  Created by alex on 11/21/12.
//  Copyright (c) 2012 alex. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate> {
    IBOutlet NSView *tableView;
    IBOutlet NSView *importView;
    NSView *viewContext;
}

@property NSArray* filesPath;
@property (retain) NSMutableArray* data;
@property (nonatomic, unsafe_unretained) IBOutlet NSWindow *window;

+ (AppDelegate *)getAppDelegate;
- (IBAction)openExistingDocument:(id)sender;
- (void)initParsing:(NSArray*)url;
- (void)showNoFileSeletedDialog;

- (IBAction)showTableView:(id)sender;
- (IBAction)showImportView:(id)sender;
- (IBAction)showPlotView:(id)sender;
- (IBAction)showSettingsView:(id)sender;

@end

