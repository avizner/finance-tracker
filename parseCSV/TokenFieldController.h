//
//  TokenFieldController.h
//  parseCSV
//
//  Created by alex on 12/26/12.
//  Copyright (c) 2012 alex. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface TokenFieldController : NSTokenFieldCell <NSTokenFieldCellDelegate>

@end
