//
//  m
//  parseCSV
//
//  Created by alex on 12/5/12.
//  Copyright (c) 2012 alex. All rights reserved.
//

#import "Payment.h"

@implementation Payment

@synthesize account;
@synthesize operationDate;
@synthesize beneficiaryAccount;
@synthesize userName;
@synthesize operation;
@synthesize amount;
@synthesize receiptCode;
@synthesize description;
@synthesize currency;
@synthesize category;

- (id)initWithArray:(NSArray*)array {
    
    if (self = [super init]) {
        
        if(array && array.count >= 15) {
            
            account             = [self sanitizeData:array[0]];
            operationDate       = [self sanitizeData:array[2]];
            beneficiaryAccount  = [self sanitizeData:array[3]];
            userName            = [self sanitizeData:array[4]];
            operation           = [self sanitizeData:array[7]];
            amount              = [self sanitizeData:array[8]];
            receiptCode         = [self sanitizeData:array[10]];
            description         = [self sanitizeData:array[11]];
            currency            = [self sanitizeData:array[13]];
            category            = nil;
            
        } else {
            return nil;
        }
        
    }
    return self;
}

- (id)sanitizeData:(id)data {
    return [data stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (id)initWithDictionary:(NSDictionary *)dict {
    if (self = [super init]) {
        account                 = [dict valueForKey:@"account"];
        operationDate           = [dict valueForKey:@"operationDate"];
        beneficiaryAccount      = [dict valueForKey:@"beneficiaryAccount"];
        userName                = [dict valueForKey:@"userName"];
        operation               = [dict valueForKey:@"operation"];
        amount                  = [dict valueForKey:@"amount"];
        receiptCode             = [dict valueForKey:@"receiptCode"];
        description             = [dict valueForKey:@"description"];
        currency                = [dict valueForKey:@"currency"];
    }
    return self;
}

- (BOOL)writePaymentToFile:(NSString*)path atomically:(BOOL)flag {
    NSLog(@"writeToFile:%@", path);
    return [[self plistStorage] writeToFile:path atomically:flag];
}

- (NSDictionary*)plistStorage
{
    NSMutableDictionary *store = [[NSMutableDictionary alloc] initWithCapacity:14];    
    [store setObject:[self account] forKey:@"account"];
    [store setObject:[self operationDate] forKey:@"operationDate"];
    [store setObject:[self beneficiaryAccount] forKey:@"beneficiaryAccount"];
    [store setObject:[self userName] forKey:@"userName"];
    [store setObject:[self amount] forKey:@"amount"];
    [store setObject:[self receiptCode] forKey:@"receiptsCode"];
    [store setObject:[self description] forKey:@"description"];
    [store setObject:[self currency] forKey:@"currency"];
    
    return [NSDictionary dictionaryWithDictionary:store];
}

@end
