//
//  NSDictionary+PaymentData.m
//  parseCSV
//
//  Created by alex on 12/24/12.
//  Copyright (c) 2012 alex. All rights reserved.
//

#import "NSDictionary+PaymentData.h"

@implementation NSDictionary (PaymentData)

+ (NSDictionary*)dictionaryFromCSVLine:(NSString*)csvLine {
    NSMutableDictionary *ret = [NSMutableDictionary dictionaryWithCapacity:8];
    NSArray *csvChunks = [csvLine componentsSeparatedByString:@","];
    
    NSDecimalNumber *account = [NSDecimalNumber decimalNumberWithString:csvChunks[0]];
    [ret setObject:account forKey:@"account"];
    
    NSDate *operationDate = [NSDate dateWithString:csvChunks[2]];
    [ret setObject:operationDate forKey:@"operationDate"];
    
    NSDecimalNumber *beneficiaryAccount = [NSDecimalNumber decimalNumberWithString:csvChunks[3]];
    [ret setObject:beneficiaryAccount forKey:@"beneficiaryAccount"];
    
    NSString *userName = [NSString stringWithString:csvChunks[4]];
    [ret setObject:userName forKey:@"userName"];
    
    NSString *operation = [NSString stringWithString:csvChunks[7]];
    [ret setObject:operation forKey:@"operation"];
    
    NSDecimalNumber *amount = [NSDecimalNumber decimalNumberWithString:csvChunks[8]];
    [ret setObject:amount forKey:@"amount"];
    
    NSDecimalNumber *receiptsCode = [NSDecimalNumber decimalNumberWithString:csvChunks[10]];
    [ret setObject:receiptsCode forKey:@"receiptsCode"];
    
    NSString *description = [NSString stringWithString:csvChunks[11]];
    [ret setObject:description forKey:@"description"];
    
    NSString *currency = [NSString stringWithString:csvChunks[13]];
    [ret setObject:currency forKey:@"currency"];
    
    return [NSDictionary dictionaryWithDictionary:ret];
}

@end
