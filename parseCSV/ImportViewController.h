//
//  ImportViewController.h
//  parseCSV
//
//  Created by alex on 12/21/12.
//  Copyright (c) 2012 alex. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ImportViewController : NSView <NSDraggingDestination, NSPasteboardItemDataProvider> {
    BOOL  highlight;
    BOOL ok;
    NSArray *urls;
    NSImage *bgImage;
    NSColor *bgColor;
}

@property IBOutlet NSTextField *label;

@end
