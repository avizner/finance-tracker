//
//  Payment.h
//  parseCSV
//
//  Created by alex on 12/5/12.
//  Copyright (c) 2012 alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Payment : NSObject {

    NSDecimalNumber     *account;                   // Current bank account
    NSDate              *operationDate;             // Date of bank transaction
    NSDecimalNumber     *beneficiaryAccount;        // Beneficiary account
    NSString            *userName;                  // Beneficiary's name
    NSString            *operation;                 // Debited or credited
    NSNumber            *amount;                    // Amount of transaction
    NSString            *receiptCode;               // Bank number of receipt
    NSString            *description;               // Description of transaction
    NSString            *currency;                  // Currency short name
    NSString            *category;                  // Indicates type of payment
}

@property NSNumber      *account;
@property NSDate        *operationDate;
@property NSNumber      *beneficiaryAccount;
@property NSString      *userName;
@property NSString      *operation;
@property NSNumber      *amount;
@property NSString      *receiptCode;
@property NSString      *description;
@property NSString      *currency;
@property NSString      *category;

- (id)initWithArray:(NSArray *)array;
- (id)initWithDictionary:(NSDictionary*)dict;
- (BOOL)writePaymentToFile:(NSString*)path atomically:(BOOL)flag;
- (NSDictionary*)plistStorage;
- (id)sanitizeData:(id)data;

@end
