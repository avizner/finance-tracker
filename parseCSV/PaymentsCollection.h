//
//  PaymentsCollection.h
//  parseCSV
//
//  Created by alex on 12/6/12.
//  Copyright (c) 2012 alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaymentsCollection : NSObject

@property NSMutableArray *list;

@end
