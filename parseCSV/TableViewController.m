//
//  TableViewController.m
//  parseCSV
//
//  Created by alex on 12/5/12.
//  Copyright (c) 2012 alex. All rights reserved.
//

#import "TableViewController.h"
#import "Payment.h"
#import "AppDelegate.h"

@implementation TableViewController

- (id)init {
    self = [super init];
    if(self) {
        data = [[NSMutableArray alloc] init];
        [self setAllowsEmptySelection:YES];
        //[self setDoubleAction:@selector(doubleAction)];
    }
    return self;
}

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    return [data count];
}

-(id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    Payment *pay = data[row];
    NSString *identifier = [tableColumn identifier];
	NSMutableDictionary *attributes = [[NSMutableDictionary alloc] init];
    NSColor *textColor = nil;
    NSString *prefix = nil;

    // Set text color for operations
	if ([[tableColumn identifier] isEqualToString:@"amount"]) {
        if ([[pay valueForKey:@"operation"] isEqualToString:@"D"]) {
            textColor = [NSColor redColor];
            prefix = @"- ";
        } else if ([[pay valueForKey:@"operation"] isEqualToString:@"C"]){
            prefix = @"+ ";
            textColor = [NSColor colorWithCalibratedRed:0.1 green:0.3 blue:0.0 alpha:1.0];
        } else {
            prefix = @"";
            textColor = [NSColor blackColor];
        }
        
        [attributes setObject:textColor forKey:NSForegroundColorAttributeName];
		return [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",
                                                           prefix,
                                                           [pay valueForKey:identifier] ]
                                               attributes:attributes];
    }

    return [pay valueForKey:identifier];
}

- (void)tableView:(NSTableView *)aTableView setObjectValue:(id)anObject forTableColumn:(NSTableColumn *)aTableColumn row:(NSInteger)rowIndex
{
    NSLog(@"set");
    NSString *columnIdentifer = [aTableColumn identifier];
    if ([columnIdentifer isEqualToString:@"operationDate"]) {
        [data replaceObjectAtIndex:rowIndex withObject:anObject];
    }
    
}

- (IBAction)add:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    data = [appDelegate data];
    NSLog(@"Populate!");
    [tView setDataSource:self];
    [tView reloadData];
}

@end
