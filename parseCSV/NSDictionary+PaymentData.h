//
//  NSDictionary+PaymentData.h
//  parseCSV
//
//  Created by alex on 12/24/12.
//  Copyright (c) 2012 alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (PaymentData)

+ (NSDictionary*)dictionaryFromCSVLine:(NSString*)csvLine;

@end
