//
//  testPayment.m
//  parseCSV
//
//  Created by alex on 12/14/12.
//  Copyright (c) 2012 alex. All rights reserved.
//

#import "testPayment.h"
#import "Payment.h"

@implementation testPayment

- (void)setUp
{
    [super setUp];
    if([self loadDataFromFile:@"../Data/test_data.csv"])
    {
        NSLog(@"Unable to load test data.");
    }
}

- (void)tearDown
{
    [super tearDown];
}

- (void)test
{
    Payment *payment;
    id mock = [OCMockObject partialMockForObject:payment];
    [[mock stub] andCall:@selector(fakeInitFromArray:) onObject:self];
    //payment = [self fakeInitFromArray:data];
    STAssertNotNil(payment, nil);
}

- (Payment*)fakeInitFromArray:(NSArray*)arr
{
    return (arr) ? [[Payment alloc] initFromArray:arr] : nil;
}

// Helpers

- (BOOL)loadDataFromFile:(NSString*)file_location
{
    NSURL *file_path = [NSURL fileURLWithPath:file_location];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[file_path path]])
    {
        return NO;
    }
    
    data = [NSString stringWithContentsOfURL:file_path encoding:NSISOLatin1StringEncoding error:nil];
    return YES;
}


@end
