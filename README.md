#####################
#   Alex Vizner     #
#       2012        #
#####################

Notes:
The application is simple finance tracker for personal usage.
The source data is files in CSV format. The main porpose of the app is
learning of Cocoa framework. 

Licensing:
You can use this app "as is" without any limitations.

Queations:
Please mail me: alexandr.vizner@gmail.com
